/************************************************************************
**
**  GRAMMAR FOR PARSING COMET CKEDITOR LOGIC AND RETURNING JSONLOGIC
**
************************************************************************/
{
  function makeNumber(o){
    var n = o[0].join("");
    if( o[1] !== null ){
      n = n + o[1];
    }
    return n*1;
  }
  function makeNode(key, val ){
    var node = {};
    node[key] = val;
    return node
  }
}

start
  = logicalnode

addOpers
  = '<arithmeticoperator>' [\ ]* operator:("+"/"-") [\ ]* "</arithmeticoperator>" { return operator }

multOpers
  = '<arithmeticoperator>' [\ ]* operator:("*"/"/") [\ ]* "</arithmeticoperator>" { return operator }

logicOpers
  = operator:(logicalAnd/logicalOr) { return operator }

logicalAnd
  = '<logicaloperator>' [\ ]* operator:"AND" [\ ]* "</logicaloperator>" { return "and" }

logicalOr
  = '<logicaloperator>' [\ ]* operator:"OR" [\ ]* "</logicaloperator>" { return "or" }

dualFunctions
  = '<function>' _ operator:(left) _ OpenParen _ param1:(variable) Separator param2:(integer) _ CloseParen _ "</function>" { return makeNode( operator, [ param1, param2 ]) }
tripleFunctions
  = '<function>' _  operator:(substr) _  OpenParen _  param1:variable _ Separator _ param2:integer _ Separator _  param3:integer _ CloseParen _ '</function>' { return makeNode( operator, [param1, param2, param3] )}
singleFunctions
  = '<function>' _ operator:(length) _ OpenParen param1:variable _ CloseParen _ "</function>" { return makeNode( operator, [ param1 ] )}
dualOperators
  = '<stringoperator>' [\ ]* operator:(isEqualTo/isNotEqualTo/isGreaterThanEqualTo/isGreaterThan/isLessThanEqualTo/isLessThan/matches/startsWith) [\ ]* "</stringoperator>" { return operator }
singleOperators
  = '<stringoperator>' [\ ]* operator:(isBlank/isNotBlank/isPresent/isNotPresent/isSelected) [\ ]* "</stringoperator>" { return operator }
prelistOperators
  = '<stringoperator>' [\ ]* operator:(isModified/isNotModified) [\ ]* "</stringoperator>" { return operator }
length
 = "<lengthtoken>length</lengthtoken>" { return "length" }
isBlank
 = "is blank" { return "isBlank" }

isNotBlank
 = "is not blank" { return "isNotBlank"; }

isEqualTo
 = "is equal to" { return "=="; }

isNotEqualTo
 = "is not equal to" { return "!="; }

isGreaterThan
 = "is greater than" { return ">"; }

left
  ="<lefttoken>left</lefttoken>" { return "left"; }

substr
  ="<substringtoken>substr</substringtoken>" { return "substr"; }

isGreaterThanEqualTo
 = "is greater than or equal to" { return ">="; }

isLessThan
 = "is less than" { return "<"; }

isLessThanEqualTo
 = "is less than or equal to" { return "<="; }

matches
 = "matches" { return "matches"; }

isModified
 = "is modified" { return "!="; }

isNotModified
 = "is not modified" { return "==";}

isPresent
 = "is present" { return "isPresent" }

isNotPresent
 = "is not present" { return "isNotPresent" }

isSelected
 = "is selected" { return "isSelected" }

startsWith
 = "starts with" { return "startsWith"}

logicalnode
  =( left:(tripleFunctions/dual) operator:logicOpers right:logicalnode ) { return makeNode(operator, [ left, right ] ) }
  /tripleFunctions/dual/single/prelist

dual
  =( left:additive operator:dualOperators right:additive ) { return makeNode(operator, [ left, right ] ) }
  /dualFunctions
  /single/prelist

single
  =( left:variable operator:singleOperators ){ return makeNode( operator, [ left ] )}
  /singleFunctions
  /prelist
  /additive
prelist
  =( left:variable operator:prelistOperators ){ return makeNode( operator, [ left, { var: "prelist." + left.var.replace(/^model\./, '') }])}
  /additive
additive
  =( left:multiplicative operator:addOpers right:additive ) {return makeNode(operator, [ left, right] )}
  /multiplicative

multiplicative
  =( left:primary operator:multOpers right:multiplicative ) { return makeNode(operator, [ left, right]) }
  / primary

primary
  = number/variable/text
  /( '<bracket>(</bracket>' logicalnode:logicalnode '<bracket>)</bracket>' ) { return logicalnode; }

text "text"
  =( '<textblock>' [\ ]*  string:([0-9a-zA-Z\-\(\)]+) [\ ]* "</textblock>" ) { return string.join(""); }

number "number"
  =( '<textblock>' [\ ]* numbers:([0-9]+decimal?) [\ ]* "</textblock>" ) { return makeNumber(numbers); }

decimal "decimal"
  = point:"." precision:[0-9]+ {return point + precision.join(""); }
integer "integer"
  = "<integer>" number:[0-9]+ "</integer>" { return number.join("")*1 }
variable "variable"
  =( '<dataelement>' [\ ]* chars:[a-zA-Z\._0-9]+ [\ ]* "</dataelement>") { return { var: "model." + chars.join("")}; }
WhiteSpace "whitespace"
  ="\t"
  /" "
_
  = (WhiteSpace)*
OpenParen
  ="<openparen>" _ "(" _ "</openparen>" { return "(" }
CloseParen
  ="<closeparen>" _ ")" _ "</closeparen>" { return ")" }
Separator
  ="<comma>" _ "," _ "</comma>" { return "," }
