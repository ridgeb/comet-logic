var express = require('express'),
    router  = express.Router(),
    fs      = require('fs'),
    path    = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET the grammar file */
router.get('/grammar', function( req, res, next ){
  fs.readFile( path.join( __dirname, '..', 'grammar', 'comet-grammar.js'), {encoding: 'utf8'}, function(err, data){
    if( err ){
      next(err);
    }
    else{
      res.json({ grammar: data });
    }
  })
})
module.exports = router;
