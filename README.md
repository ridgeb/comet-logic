# comet-logic
All things COMET logic related (scripts, grammar, etc)

### Grammar
[Grammar](grammar/comet-grammar.js) File

Notes: I think the only things I've added that I really need from you are:

* adding `model.` to variables
```
variable "variable"
  =('<dataelement>' [\ ]* chars:[a-zA-Z\.0-9]+ [\ ]* "</dataelement>){ return { var: "model." + chars.join("")}; }
```

* adding prelist "stuff" -- for `is modified` and `is not modified`. Centurion will store all prelisted data points in `$scope.prelist`. `is modified` and `is not modified` will turn into `model.NAME == prelist.NAME` and `model.name != prelist.NAME`.

### Using PEGJS in browser js

`bower install pegjs`

Including peg-0.9.0.min.js into your html
```
<script src="pegjs/peg-0.9.0.min.js"></script>
```
Will let you call the following in your other js (you'll have to get the grammar in there somehow)
```
var parser = PEG.buildParser( grammar );

parser.parse( cometXML );
```
