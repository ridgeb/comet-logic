
angular.module( "jsonlogicApp", [] )
.controller( 'jsonlogicCtrl', function( $scope, $http ){
  $scope.model = {};
  $scope.prior = {};
  var parser = "";

  $http.get( "/grammar" ).success(function(data){
    parser = PEG.buildParser( data.grammar );
  }).error(function(data){
    alert( "ERROR: unable to read grammar file")
  })
  $scope.parse = function(){
    var jsonlogicCode = "";
    try {
      jsonlogicCode = parser.parse( $scope.cometCode );
    } catch( e ){
      alert( e.message );
      jsonlogicCode = "Error Parsing";
    }
    $scope.jsonlogicCode = jsonlogicCode;
    $scope.jsonlogic = JSON.stringify( jsonlogicCode, null, 2 );
    $scope.varHash = {}
    findVars( jsonlogicCode, function(key){
      $scope.varHash[key] = 1
    });
    $scope.result = validate($scope.jsonlogicCode, $scope.model );
  }
  $scope.modelfun = function(){ return JSON.stringify( { model: $scope.model, prior: $scope.prior } )}
  $scope.$watch('modelfun()', function(newValue ){
    var model = JSON.parse( newValue );
    $scope.result = validate($scope.jsonlogicCode, { model: $scope.model, prior: $scope.prior });
  })
  $scope.varfun = function(){ return JSON.stringify( $scope.varHash )}
  $scope.$watch('varfun()',function(newValue){
    if( newValue !== undefined){
      var hash = JSON.parse( newValue )
      $scope.vars = [];
      $scope.priors = [];
      for( var key in hash ){
        if( key.match( /^prior\./ )){
          $scope.priors.push( { varname: key, key: key.replace( /^prior\./, '') } );
        }
        else{
          $scope.vars.push( { varname: key, key: key.replace( /^model\./, '' ) } );
        }
      }
    }
  });
})
function validate(logic, data){
  return jsonLogic.apply( logic, data);
}
function findVars( json, setVar ){
 for( var key in json ){
  if( key == "var" ){
    setVar(json[key]);
  }
  else if( typeof json[key] == "object" ){
    findVars( json[key], setVar)
  }
 }
}
