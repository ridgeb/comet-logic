var assert = require('chai').assert,
    PEGJS  = require('pegjs'),
    fs     = require('fs'),
    path   = require('path');

var grammar = fs.readFileSync( path.join(__dirname, "..", "grammar", "comet-grammar.js" ), "utf8" );

var parser = PEGJS.buildParser( grammar );

describe("Logical Operators", function(){
  describe( "or", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><logicaloperator>OR</logicaloperator><textblock>1</textblock>") 
      assert.deepEqual({ "or": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<logicaloperator>OR</logicaloperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><logicaloperator>OR</logicaloperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "and", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><logicaloperator>AND</logicaloperator><textblock>1</textblock>") 
      assert.deepEqual({ "and": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<logicaloperator>AND</logicaloperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><logicaloperator>AND</logicaloperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
});
describe("Math Operators", function(){
  describe("+", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><arithmeticoperator>+</arithmeticoperator><textblock>1</textblock>") 
      assert.deepEqual({ "+": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<arithmeticoperator>+</arithmeticoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><arithmeticoperator>+</arithmeticoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe("-", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><arithmeticoperator>-</arithmeticoperator><textblock>1</textblock>") 
      assert.deepEqual({ "-": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<arithmeticoperator>-</arithmeticoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><arithmeticoperator>-</arithmeticoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe("*", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><arithmeticoperator>*</arithmeticoperator><textblock>1</textblock>") 
      assert.deepEqual({ "*": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<arithmeticoperator>*</arithmeticoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><arithmeticoperator>*</arithmeticoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe("/", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><arithmeticoperator>/</arithmeticoperator><textblock>1</textblock>") 
      assert.deepEqual({ "/": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<arithmeticoperator>/</arithmeticoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><arithmeticoperator>/</arithmeticoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
});
describe("Basic Operators", function(){
  describe( "is equal to", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is equal to</stringoperator><textblock>1</textblock>") 
      assert.deepEqual({ "==": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is equal to</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is equal to</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is not equal to", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is not equal to</stringoperator><textblock>1</textblock>") 
      assert.deepEqual({ "!=": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is not equal to</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is not equal to</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is greater than", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is greater than</stringoperator><textblock>1</textblock>") 
      assert.deepEqual({ ">": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is greater than</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is greater than</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is greater than or equal to", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is greater than or equal to</stringoperator><textblock>1</textblock>") 
      assert.deepEqual({ ">=": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is greater than or equal to</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is greater than or equal to</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is less than", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is less than</stringoperator><textblock>1</textblock>") 
      assert.deepEqual({ "<": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is less than</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is less than</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is less than or equal to", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is less than or equal to</stringoperator><textblock>1</textblock>") 
      assert.deepEqual({ "<=": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is less than or equal to</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is less than or equal to</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is blank", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is blank</stringoperator>") 
      assert.deepEqual({ "isBlank": [ { var: "model.X" } ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is blank</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is not blank", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is not blank</stringoperator>") 
      assert.deepEqual({ "isNotBlank": [ { var: "model.X" } ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is not blank</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "matches", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>matches</stringoperator><textblock>abc</textblock>") 
      assert.deepEqual({ "matches": [ { var: "model.X" }, "abc" ] }, jsonlogic );
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>matches</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>matches</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "starts with", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>starts with</stringoperator><textblock>abc</textblock>") 
      assert.deepEqual({ "startsWith": [ { var: "model.X" }, "abc" ] }, jsonlogic );
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>starts with</stringoperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>starts with</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is modified", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is modified</stringoperator>") 
      assert.deepEqual({ "!=": [ { var: "model.X" }, { var: "prelist.X" } ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is modified<stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is not modified", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is not modified</stringoperator>") 
      assert.deepEqual({ "==": [ { var: "model.X" }, { var: "prelist.X"} ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is not modified</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  }) 
  describe( "is present", function(){
    it('should parse valid use of the operator -- FIXME: still not sure what this operator is supposed to do!!', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><stringoperator>is present</stringoperator>")
      assert.deepEqual({ "isPresent": [ {var: "model.X"}]}, jsonlogic );;
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is present<stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "is not present", function(){
    it('should parse valid use of the operator -- FIXME: still not sure what this operator is supposed to do!!', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is not present</stringoperator>") 
      assert.deepEqual({ "isNotPresent": [ { var: "model.X" } ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is not present</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
  describe( "substr", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<function><substringtoken>substr</substringtoken><openparen>(</openparen><dataelement>X</dataelement><comma>,</comma><integer>1</integer><comma>,</comma><integer>3</integer><closeparen>)</closeparen></function>") 
      assert.deepEqual({ "substr": [ { var: "model.X" },1,3 ] }, jsonlogic );
    })
  })
  describe( "left", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<function><lefttoken>left</lefttoken><openparen>(</openparen><dataelement>X</dataelement><comma>,</comma><integer>3</integer><closeparen>)</closeparen></function>" );
      assert.deepEqual({ "left": [ { var: "model.X" },3 ] }, jsonlogic );
    })
  })
  describe( "length", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<function><lengthtoken>length</lengthtoken><openparen>(</openparen><dataelement>X</dataelement><closeparen>)</closeparen></function>") 
      assert.deepEqual({ "length": [ { var: "model.X" }] }, jsonlogic );
    })
  })
  describe( "is selected", function(){
    it('should parse valid use of the operator -- FIXME: still not sure what this operator is supposed to do!!', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><stringoperator>is selected</stringoperator>") 
      assert.deepEqual({ "isSelected": [ { var: "model.X" } ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<stringoperator>is selected</stringoperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
  }) 
})

describe("Logical Operators", function(){
  describe( "or", function(){
    it('should parse valid use of the operator', function(){
      var jsonlogic = parse( "<dataelement>X</dataelement><logicaloperator>OR</logicaloperator><textblock>1</textblock>") 
      assert.deepEqual({ "or": [ { var: "model.X" }, 1 ] }, jsonlogic );
    })
    it('should fail use of the operator without a left parameter', function(){
      var jsonlogic = parse("<logicaloperator>OR</logicaloperator><dataelement>X</dataelement>");
      assert.equal("Error Parsing", jsonlogic);
    })
    it('should fail use of the operator without a right parameter', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><logicaloperator>OR</logicaloperator>");
      assert.equal("Error Parsing", jsonlogic);
    })
  })
});

describe( "Order of Operations", function(){
  describe( "Math Operators take precedence over Logical Operators", function(){
    it('+ should take precedence over OR', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><logicaloperator>OR</logicaloperator><dataelement>Y</dataelement><arithmeticoperator>+</arithmeticoperator><textblock>1</textblock>");
      assert.deepEqual({"or": [ {var: "model.X"}, { "+": [ {var: "model.Y"}, 1]}]}, jsonlogic)
    })
    it('* should take precedence over AND', function(){
      var jsonlogic = parse("<dataelement>X</dataelement><logicaloperator>AND</logicaloperator><dataelement>Y</dataelement><arithmeticoperator>*</arithmeticoperator><textblock>1</textblock>");
      assert.deepEqual({"and": [ {var: "model.X"}, { "*": [ {var: "model.Y"}, 1]}]}, jsonlogic)
    })
    it('brackets have highest precedence',function(){
      var jsonlogic = parse("<bracket>(</bracket><dataelement>X</dataelement><logicaloperator>AND</logicaloperator><dataelement>Y</dataelement><bracket>)</bracket><arithmeticoperator>*</arithmeticoperator><textblock>1</textblock>");
      assert.deepEqual({"*": [ { "and": [ {var: "model.X"}, {var: "model.Y"}]},1]}, jsonlogic)
    })
  });
})
function parse( xml ){
  var jsonlogic;
  try { 
    jsonlogic = parser.parse( xml );
  } catch(e){
    jsonlogic = "Error Parsing";
  }
  return jsonlogic;
}
