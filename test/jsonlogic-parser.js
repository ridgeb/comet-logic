var assert = require('chai').assert,
    jsonlogic = require('../lib/comet-jsonlogic-parser.js');
describe("Math Operators", function(){
  describe( "+", function(){
    it("should recognize and evaluate the operator correctly", function(){
      assert.equal( 3, jsonlogic.apply({"+": [1,2]}))
    })
  })
  describe( "-", function(){
    it("should recognize and evaluate the operator correctly", function(){
      assert.equal( 1, jsonlogic.apply({"-": [3,2]}))
    })
  })
  describe( "*", function(){
    it("should recognize and evaluate the operator correctly", function(){
      assert.equal( 6, jsonlogic.apply({"*": [3,2]}))
    })
  })
  describe( "/", function(){
    it("should recognize and evaluate the operator correctly", function(){
      assert.equal( 2, jsonlogic.apply({"/": [6,3]}))
    })
  })
})
describe("Custom COMET Operators", function(){
  describe( "isBlank", function(){
    it("should recognize and evaluate the operator correctly (true)", function(){
      assert.equal(true, jsonlogic.apply({isBlank: [{var: 'X'}]}, { X: "" }))
    })
    it("should recognize and evaluate the operator correctly (false)", function(){
      assert.equal(false, jsonlogic.apply({isBlank: [{var: 'X'}]}, { X: "a" }))
    })
  })
  describe( "isNotBlank", function(){
    it("should recognize and evaluate the operator correctly (true)", function(){
      assert.equal(true, jsonlogic.apply({isNotBlank: [{var: 'X'}]}, { X: "a" }))
    })
    it("should recognize and evaluate the operator correctly (false)", function(){
      assert.equal(false, jsonlogic.apply({isNotBlank: [{var: 'X'}]}, { X: "" }))
    })
  })
  describe( "matches", function(){
    it("should recognize and evaluate the operator correctly (true)", function(){
      assert.equal(true, jsonlogic.apply({matches: [{var: 'X'}, "bc"]}, { X: "abcde" }))
    })
    it("should recognize and evaluate the operator correctly (false)", function(){
      assert.equal(false, jsonlogic.apply({matches: [{var: 'X'}, "bc"]}, { X: "abde" }))
    })
  })
  describe( "startsWith", function(){
    it("should recognize and evaluate the operator correctly (true)", function(){
      assert.equal(true, jsonlogic.apply({matches: [{var: 'X'}, "abc"]}, { X: "abcde" }))
    })
    it("should recognize and evaluate the operator correctly (false)", function(){
      assert.equal(false, jsonlogic.apply({matches: [{var: 'X'}, "abc"]}, { X: "abde" }))
    })
  })
  describe( "substr", function(){
    it("should recognize and evaluate the operator correctly when substring appears in the middle of the string", function(){
      assert.equal("abc", jsonlogic.apply({substr: [{var: 'X'}, 4, 3]}, { X: "xyzabcde" }))
    })
    it("should recognize and evaluate the operator correctly when substring appears at the beginning of the string", function(){
      assert.equal("abc", jsonlogic.apply({substr: [{var: 'X'}, 1, 3]}, { X: "abc" }))
    })
    it("should recognize and evaluate the operator correctly when substring appears at the end of the string", function(){
      assert.equal("abc", jsonlogic.apply({substr: [{var: 'X'}, 4, 3]}, { X: "xyzabc" }))
    })
    it("should recognize and evaluate the operator correctly when substring runs through the end of the string", function(){
      assert.equal("abc", jsonlogic.apply({substr: [{var: 'X'}, 4, 22]}, { X: "xyzabc" }))
    })
    it("should recognize and evaluate the operator correctly when no substring length is given", function(){
      assert.equal("abc", jsonlogic.apply({substr: [{var: 'X'}, 4]}, { X: "xyzabc" }))
    })
    it("should recognize and return an empty string when start point or length == 0 or are undefined", function(){
      assert.equal("", jsonlogic.apply({substr: [{var: 'X'}, 0, 0]}, { X: "abcdef"}))
    })
  })
  describe( "left", function(){
    it("should recognize and evaluate the operator correctly when the length <= length of the string", function(){
      assert.equal("abc", jsonlogic.apply({left: [{var: 'X'}, 3]}, { X: "abcde" }))
    })
    it("should recognize and evaluate the operator correctly when the length > length of the string", function(){
      assert.equal("abc", jsonlogic.apply({left: [{var: 'X'}, 20]}, { X: "abc" }))
    })
  })
  describe( "length", function(){
    it("should recognize and evaluate the operator correctly when the length > 0", function(){
      assert.equal(3, jsonlogic.apply({length: [{var: 'X'}]}, { X: "abc" }))
    })
    it("should recognize and evaluate the operator correctly when the length = 0", function(){
      assert.equal(0, jsonlogic.apply({length: [{var: 'X'}]}, { X: null }))
    })
  })
})

